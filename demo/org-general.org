This org file is a configuration file that is read on startup. Some of these configuration options need to be cleaned up. 

NOTE: If something does not work, the option may be loaded in another config file and overriding these settings. Check ~/.emacs.d/init.el , ~/.spacemacs and other config files as applicable. 

* Configurations for Spacemacs 

    #+BEGIN_EXAMPLE emacs-lisp
     org
           (org :variables
         org-enable-reveal-js-support t
         )  
    #+END_EXAMPLE

    #+BEGIN_EXAMPLE emacs-lisp
   dotspacemacs-additional-packages '(ox-reveal org-journal noflet)
    #+END_EXAMPLE

* Calfw configuration
  This is for configuration of the calendar piece in emacs

  #+BEGIN_SRC
  (require 'calfw)
  (require 'calfw-org)
  #+END_SRC
  
  ;; #+BEGIN_SRC
  ;; (setq-default dotspacemacs-configuration-layers '(calendar))
  ;; #+END_SRC

* Configure the org-directory path setting. 

*NOTE* This setting needs to be adjusted per machine. 

This configuration allows for syncing between machines with different directories.

    #+BEGIN_SRC emacs-lisp
    (if (file-directory-p "~/Nextcloud/orgmode_notes") (setq org-directory "~/Nextcloud/orgmode_notes") (setq org-directory "~/home_nextcloud/orgmode_notes"))
    #+END_SRC

** This configuration can be used when a single/same directory is used. Remove the ;; from the beginning of the line to enable it.  

    #+BEGIN_EXAMPLE emacs-lisp
    (setq org-directory "~/Nextcloud/orgmode_notes")
    #+END_EXAMPLE

* Configures the default notes file

    #+BEGIN_SRC emacs-lisp
    (custom-set-variables
       '(org-default-notes-file (concat org-directory "/notes.org"))
    )
    #+END_SRC

* This enables word wrap in Org

    #+BEGIN_SRC emacs-lisp
       (global-visual-line-mode t)
    #+END_SRC

* This configures the org files that make up the agenda

    #+BEGIN_SRC emacs-lisp
    (setq org-agenda-files
          (list
           (concat org-directory "/aplura.org")
           (concat org-directory "/home.org")
           ;; (concat org-directory "/meetings.org")))
           ))
    #+END_SRC

** This is an example of setting agenda files with a more complete path

    #+BEGIN_EXAMPLE emacs-lisp
    (setq org-agenda-files (list "~/home_nextcloud/orgmode_notes/aplura.org"
                                  "~/home_nextcloud/orgmode_notes/links.org"))
    #+END_EXAMPLE

* Configure org-mode capture templates

    #+BEGIN_SRC emacs-lisp
      (setq org-capture-templates
         `(("a" "Appointment" entry (file+headline ,(concat org-directory "/gcal.org") "Appointments")
      "* TODO %?\n:PROPERTIES:\n\n:END:\nDEADLINE: %^T \n %i\n")
      ("n" "Note" entry (file+headline ,(concat org-directory "/notes.org") "Notes")
      "* Note %?\n%T")
      ("l" "Link" entry (file+headline ,(concat org-directory "/links.org") "Links")
      "* %? %^L %^g \n%T" :prepend t)
      ("b" "Blog idea" entry (file+headline ,(concat org-directory "/i.org") "Blog Topics:")
      "* %?\n%T" :prepend t)
      ("t" "To Do Item" entry (file+headline ,(concat org-directory "/i.org") "To Do Items")
      "* %?\n%T" :prepend t)
      ("j" "Journal" entry (file+datetree ,(concat org-directory "/journal.org"))
      "* %?\nEntered on %U\n  %i\n  %a")
      ("m" "Meeting Capture" entry (file+headline ,(concat org-directory "/meeting_notes.org") "Meeting Notes")
      "* %?\n:DATE: %^T \n*** ATTENDEES:\n\n*** TOPIC:\n\n*** NOTES:\n%i")
      ("c" "Splunk Sample Capture" entry (file+headline ,(concat org-directory "/splunk_searches.org") "Splunk Searches")
       "* REFILE %? \n    #+BEGIN_EXAMPLE\n\n    #+END_EXAMPLE\n** NOTES:\n%i")
      ("s" "Screencasts" entry (file ,(concat org-directory "/screencastnotes.org"))
      "* %?\n%i\n")
      ))
    #+END_SRC

* Orgmode capture window

There are multiple methods here. I think the one in use is the best option, but leaving the other for reference.

** Following found on https://cestlaz-nikola.github.io/posts/using-emacs-24-capture-2/

    #+BEGIN_EXAMPLE emacs-lisp
     (defadvice org-capture-finalize
         (after delete-capture-frame activate)
       "Advise capture-finalize to close the frame"
       (if (equal "capture" (frame-parameter nil 'name))
         (delete-frame)))
    
     (defadvice org-capture-destroy
         (after delete-capture-frame activate)
       "Advise capture-destroy to close the frame"
       (if (equal "capture" (frame-parameter nil 'name))
         (delete-frame)))
    
     (use-package noflet
       :ensure t )
     (defun make-capture-frame ()
       "Create a new frame and run org-capture."
       (interactive)
       (make-frame '((name . "capture")))
       (select-frame-by-name "capture")
       (delete-other-windows)
       (noflet ((switch-to-buffer-other-window (buf) (switch-to-buffer buf)))
         (org-capture)))
    #+END_EXAMPLE

** Found in comments from https://cestlaz-nikola.github.io/posts/using-emacs-24-capture-2/

    #+BEGIN_SRC emacs-lisp
     (defun my-org-capture ()
      "Create a new frame and run `org-capture'."
      (interactive)
      (select-frame (make-frame '((my-org-capture . t))))
      (delete-other-windows)
      (cl-letf (((symbol-function 'switch-to-buffer-other-window) #'switch-to-buffer))
        (condition-case err
            (org-capture)
          ;; `org-capture' signals (error "Abort") when "q" is typed, so
          ;; delete the newly-created frame in this scenario.
          (error (when (equal err '(error "Abort"))
                   (delete-frame)))))
     
     (defadvice org-capture-finalize (after my-delete-capture-frame activate)
      "Delete the frame after `capture-finalize'."
      (when (frame-parameter nil 'my-org-capture)
        (delete-frame)))
     
     (defadvice org-capture-destroy (after my-delete-capture-frame activate)
      "Delete the frame after `capture-destroy'."
      (when (frame-parameter nil 'my-org-capture)
         (delete-frame))))
    #+END_SRC

* Create refile targets in Org Mode

** Found at [[Stackoverflow refile question][]https://stackoverflow.com/questions/22200312/refile-from-one-file-to-other]] 

    #+BEGIN_SRC emacs-lisp
    (setq org-refile-targets
          '(("home.org" :maxlevel . 3)
            ("aplura.org" :maxlevel . 3)))
    #+END_SRC

** This is another way to do it using the `org-agenda-files' inforamtion as the target files

    #+BEGIN_EXAMPLE emacs-lisp
    ;; (setq org-refile-targets
    ;;       '((nil :maxlevel . 3)
    ;;         (org-agenda-files :maxlevel . 3)))
    #+END_EXAMPLE

* For exporting to reveal documents

*NOTE* This setting needs to be adjusted per machine.

    #+BEGIN_SRC emacs-lisp
    ;; (setq org-reveal-root "file:///home/chuck/Nextcloud/orgmode_notes/reveal.js")
    (setq org-reveal-root "file:///Users/chuck/home_nextcloud/orgmode_notes/reveal.js")
    ;;(setq org-reveal-root (concat org-directory "/reveal.js"))
     (setq-default dotspacemacs-configuration-layers
                   '((org :variables org-enable-reveal-js-support t)))
    #+END_SRC

** Some notes on the reveal code that is needed

    #+BEGIN_EXAMPLE
    ;; Reveal.js + Org mode
    ;; (require 'ox-reveal)
    ;; (setq org-reveal-root "file:///home/chuck/Nextcloud/orgmode_notes/reveal.js")
    ;; (setq Org-Reveal-title-slide nil)
    #+END_EXAMPLE

* Todo Keywords

This setting determines the TODO states. Instrucitons are at [[https://orgmode.org/manual/Workflow-states.html][Workflow States]]

    #+BEGIN_SRC emacs-lisp
    (setq org-todo-keywords
          '((sequence "TODO(t)" "NEXT(n)" "INPROGRESS(i)" "WAITING(w)" "MAYBE(m)" "REFILE(r)" "|" "DONE" "DELEGATED" "CANCELED")))
    #+END_SRC
* Support for Airmail links
   org-airmail.el - Support for links to Airmail 3 messages in Org
   Found at [[https://gist.github.com/ephsmith/ffbf1e75365b8d4db973bb6957548b6c][github]]

    #+BEGIN_SRC emacs-lisp
    (require 'org)
    
    (org-add-link-type "airmail" 'org-airmail-open)
    
    (defun org-airmail-open (url)
      "Visit the Airmail message referenced by URL.
    URL should be a vaid Airmail message url retrieved from Airmail with
    'Copy Message Link'."
      (shell-command
       ;; Note: org strips "airmail:" from the link URL
       (concat "open -a '/Applications/Airmail 3.app' airmail:"
    	   (shell-quote-argument url))))
    
    (provide 'org-airmail)
    #+END_SRC
* Archive DONE tasks in subtree
  This was found at [[https://stackoverflow.com/questions/6997387/how-to-archive-all-the-done-tasks-using-a-single-command][Stack overflow comment]]

  #+BEGIN_SRC emacs-lisp
  (defun my-org-archive-done-tasks ()
    (interactive)
    (org-map-entries 'org-archive-subtree "/DONE" 'file))
  #+END_SRC
* Enter notes into logbook drawer
  C-c C-z puts notes into logbook property
  
  #+BEGIN_SRC emacs-lisp
  (setq org-log-into-drawer "LOGBOOK")
  #+END_SRC

* Enable Code Execution

  #+BEGIN_SRC emacs-lisp
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((python . t)
       (shell . t)
       ))
 #+END_SRC
